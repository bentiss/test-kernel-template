#!/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import logging
import yaml


def open_testfile(path):
    with open(path) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    devices = data['devices']
    del(data['devices'])
    return devices, data


def extend_obj(item, base, obj):
    '''extends the content of obj['item'] from base['item']'''
    try:
        items = obj[item]
    except KeyError:
        items = base[item].copy()
        obj[item] = items
    else:
        for item in base[item]:
            if item not in items:
                items.append(item)


def main():
    path = 'tests.yaml'
    all_devices, tests = open_testfile(path)

    for type, definition in tests.items():
        name = type.replace('_', ' ').title()
        print(f'{name}:')
        print(f'{"=" * (len(type) + 1)}')
        print()

        devices = [d for d in all_devices if d['type'] == type]

        try:
            extends = definition['extends']
        except KeyError:
            pass
        else:
            base = tests[extends]
            extend_obj('bus', base, definition)
            extend_obj('testing', base, definition)

        try:
            busses = definition['bus']
        except KeyError:
            # general test, no devices attached
            pass
        else:
            for bus in busses:
                print(f'{bus}:')
                print(f'{"-" * (len(bus) + 1)}')
                print()

                for device in devices:
                    if bus != device['bus']:
                        continue
                    name = device['name']
                    device['used'] = True
                    print(f'- [ ] {name}:')
                    try:
                        hint = device['hint']
                    except KeyError:
                        hint = None
                    else:
                        if hint is not None:
                            print(f'      *({hint})*')

                    for test in definition['testing']:
                        print(f'  - [ ] {test}')
                    print('---')

        try:
            runs = definition['run']
        except KeyError:
            pass
        else:
            for run in runs:
                print(f'- [ ] {run}:')
                print('```bash')
                print(' ... TODO: Paste the output here ...')
                print('```')
                print('---')

    for device in all_devices:
        try:
            used = device['used']
        except KeyError:
            logging.error(f'{device} is not used')

if __name__ == '__main__':
    main()
