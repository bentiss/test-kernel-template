Generate a list of tests to run for regression tests
----------------------------------------------------

The idea is to run `generate_template.py` and create an issue on this project
with the tested kernel.
