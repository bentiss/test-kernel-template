General:
========

- [ ] hid-tools test suite:
```bash
 ... TODO: Paste the output here ...
```
---
Keyboard:
=========

PS/2:
-----

- [ ] any PS/2 keyboard on a laptop:
      *(T450s, Dell XPS 13 2-in-1)*
  - [ ] make sure we can type keys
  - [ ] kbd LEDs
  - [ ] special keys (volume, play/pause, etc...)
---
USB:
----

- [ ] Plain USB keyboard:
  - [ ] make sure we can type keys
  - [ ] kbd LEDs
  - [ ] special keys (volume, play/pause, etc...)
---
Bluetooth:
----------

BLE:
----

DJ:
---

- [ ] Logitech Solaar K750:
  - [ ] make sure we can type keys
  - [ ] kbd LEDs
  - [ ] special keys (volume, play/pause, etc...)
---
Logitech receiver:
------------------

- [ ] Logitech K270:
      *(cheapest possible wireless Logitech keyboard)*
  - [ ] make sure we can type keys
  - [ ] kbd LEDs
  - [ ] special keys (volume, play/pause, etc...)
---
Mouse:
======

PS/2:
-----

USB:
----

- [ ] Plain USB mouse:
      *(Logitech G600)*
  - [ ] cursor moves
  - [ ] buttons
  - [ ] scrolling
---
Bluetooth:
----------

BLE:
----

DJ:
---

- [ ] Logitech DJ mouse:
      *(M325)*
  - [ ] cursor moves
  - [ ] buttons
  - [ ] scrolling
---
Logitech receiver:
------------------

- [ ] Logitech Receiver mouse:
      *(M185)*
  - [ ] cursor moves
  - [ ] buttons
  - [ ] scrolling
---
High Wheel Mouse:
=================

PS/2:
-----

USB:
----

Bluetooth:
----------

BLE:
----

- [ ] Logitech MX Master BLE:
  - [ ] scrolling in high res mode
  - [ ] suspend/resume scrolling
  - [ ] cursor moves
  - [ ] buttons
  - [ ] scrolling
---
DJ:
---

- [ ] Logitech MX Master DJ:
  - [ ] scrolling in high res mode
  - [ ] suspend/resume scrolling
  - [ ] cursor moves
  - [ ] buttons
  - [ ] scrolling
---
Logitech receiver:
------------------

Trackball:
==========

PS/2:
-----

USB:
----

Bluetooth:
----------

BLE:
----

DJ:
---

- [ ] Logitech M570:
  - [ ] cursor moves
  - [ ] buttons
  - [ ] scrolling
---
Logitech receiver:
------------------

Touchpad:
=========

PS/2:
-----

- [ ] SynPS/2 touchpad:
      *(t450s with psmouse.synaptics_rmi=0, Dell XPS 13 2-in-1 without i2c-hid)*
  - [ ] touchpad edge detector
  - [ ] regular input working
  - [ ] speed
  - [ ] buttons (virtual or emulated or physical working)
  - [ ] gestures (scroll, pinch, zoom)
---
- [ ] Elantech PS/2 touchpad:
      *(P52 with psmouse.elantech_smbus=0)*
  - [ ] touchpad edge detector
  - [ ] regular input working
  - [ ] speed
  - [ ] buttons (virtual or emulated or physical working)
  - [ ] gestures (scroll, pinch, zoom)
---
USB:
----

- [ ] Wacom Intuos Pro Finger:
  - [ ] touchpad edge detector
  - [ ] regular input working
  - [ ] speed
  - [ ] buttons (virtual or emulated or physical working)
  - [ ] gestures (scroll, pinch, zoom)
---
Bluetooth:
----------

- [ ] Logitech T651:
      *(white)*
  - [ ] touchpad edge detector
  - [ ] regular input working
  - [ ] speed
  - [ ] buttons (virtual or emulated or physical working)
  - [ ] gestures (scroll, pinch, zoom)
---
DJ:
---

- [ ] Logitech T650:
      *(black)*
  - [ ] touchpad edge detector
  - [ ] regular input working
  - [ ] speed
  - [ ] buttons (virtual or emulated or physical working)
  - [ ] gestures (scroll, pinch, zoom)
---
SMBus:
------

- [ ] Synaptics RMI4 over SMBus:
      *(t450s)*
  - [ ] touchpad edge detector
  - [ ] regular input working
  - [ ] speed
  - [ ] buttons (virtual or emulated or physical working)
  - [ ] gestures (scroll, pinch, zoom)
---
- [ ] Elan over SMBus:
      *(P52)*
  - [ ] touchpad edge detector
  - [ ] regular input working
  - [ ] speed
  - [ ] buttons (virtual or emulated or physical working)
  - [ ] gestures (scroll, pinch, zoom)
---
i2c-hid:
--------

- [ ] MS PTP:
      *(Dell XPS 13 2-in-1, Dell XPS 13, HP x360 mWS)*
  - [ ] touchpad edge detector
  - [ ] regular input working
  - [ ] speed
  - [ ] buttons (virtual or emulated or physical working)
  - [ ] gestures (scroll, pinch, zoom)
---
Touchscreen:
============

USB:
----

- [ ] USB win8 touchscreen:
      *(Dell Canvas 27)*
  - [ ] touchpad edge detector
  - [ ] regular input working
  - [ ] libinput debug-gui
---
- [ ] USB Wacom touchscreen:
      *(Cintiq 13 QHD, P52)*
  - [ ] touchpad edge detector
  - [ ] regular input working
  - [ ] libinput debug-gui
---
i2c-hid:
--------

- [ ] i2c-hid Wacom touchscreen:
      *(Dell XPS 2-in-1)*
  - [ ] touchpad edge detector
  - [ ] regular input working
  - [ ] libinput debug-gui
---
Tablet Direct:
==============

USB:
----

- [ ] Wacom AES USB:
      *(P52)*
  - [ ] touchpad edge detector
  - [ ] regular input working
  - [ ] libinput debug-gui
  - [ ] ensure cursor under the tool
  - [ ] buttons on stylus
  - [ ] palm rejection (kernel or libinput)
  - [ ] wacom_settings (stylus detected, can use UI, can change buttons)
---
- [ ] Wacom Cintiq 13HD:
  - [ ] touchpad edge detector
  - [ ] regular input working
  - [ ] libinput debug-gui
  - [ ] ensure cursor under the tool
  - [ ] buttons on stylus
  - [ ] palm rejection (kernel or libinput)
  - [ ] wacom_settings (stylus detected, can use UI, can change buttons)
---
i2c-hid:
--------

- [ ] Wacom AES I2C:
      *(Dell XPS 13 2-in-1 (with proper support), Dell XPS 15 2-in-1, HP x360 mWS)*
  - [ ] touchpad edge detector
  - [ ] regular input working
  - [ ] libinput debug-gui
  - [ ] ensure cursor under the tool
  - [ ] buttons on stylus
  - [ ] palm rejection (kernel or libinput)
  - [ ] wacom_settings (stylus detected, can use UI, can change buttons)
---
Tablet Indirect:
================

USB:
----

- [ ] Wacom Intuos Pro:
  - [ ] touchpad edge detector
  - [ ] regular input working
  - [ ] libinput debug-gui
  - [ ] buttons on stylus
  - [ ] palm rejection (kernel or libinput)
  - [ ] wacom_settings (stylus detected, can use UI, can change buttons)
---
i2c-hid:
--------

Tablet Pad:
===========

USB:
----

- [ ] Wacom Cintiq 13HD Pad:
  - [ ] buttons
  - [ ] ring
  - [ ] tablet modes
  - [ ] wacom_settings (stylus detected, can use UI, can change buttons)
  - [ ] wacom OSD
---
- [ ] Wacom Intuos Pro Pad:
  - [ ] buttons
  - [ ] ring
  - [ ] tablet modes
  - [ ] wacom_settings (stylus detected, can use UI, can change buttons)
  - [ ] wacom OSD
---
- [ ] Wacom EKR:
  - [ ] buttons
  - [ ] ring
  - [ ] tablet modes
  - [ ] wacom_settings (stylus detected, can use UI, can change buttons)
  - [ ] wacom OSD
---
i2c-hid:
--------

Totem:
======

USB:
----

- [ ] Dell Totem:
      *(Dell Canvas 27)*
  - [ ] size correct
  - [ ] button works
  - [ ] rotation works
  - [ ] haptic feedback
---
BLE:
----

- [ ] Surface Dial:
  - [ ] size correct
  - [ ] button works
  - [ ] rotation works
  - [ ] haptic feedback
---
